module Main where

import           Data.Char (toUpper)
import           Data.Functor ((<&>))
import           Data.List (intercalate)
import           Data.List.NonEmpty (NonEmpty(..))
import           Data.Map (Map, fromList)
import qualified Data.Map as Map
import           Data.Maybe (fromJust)
import           System.Environment (getArgs)

data MorseSymbol = Dot | Dash
type MorseLetter = NonEmpty MorseSymbol
type MorseWord = NonEmpty MorseLetter
type MorseCode = NonEmpty MorseWord

symbol2char :: MorseSymbol -> Char
symbol2char Dot = '·'
symbol2char Dash = '-'

letter2str :: MorseLetter -> String
letter2str (first :| rest) = map symbol2char (first:rest)

word2str :: MorseWord -> String
word2str (first :| rest) = intercalate " " (map letter2str (first:rest))

mc2str :: MorseCode -> String
mc2str (first :| rest) = intercalate "   " (map word2str (first:rest))

charCodeMapping :: Map Char MorseLetter
charCodeMapping = fromList [ ('A', Dot  :| [Dash])
                           , ('B', Dash :| [Dot, Dot, Dot])
                           , ('C', Dash :| [Dot, Dash, Dot])
                           , ('D', Dash :| [Dot, Dot])
                           , ('E', Dot  :| [])
                           , ('F', Dot  :| [Dot, Dash, Dot])
                           , ('G', Dash :| [Dash, Dot])
                           , ('H', Dot  :| [Dot, Dot, Dot])
                           , ('I', Dot  :| [Dot])
                           , ('J', Dot  :| [Dash, Dash, Dash])
                           , ('K', Dash :| [Dot, Dash])
                           , ('L', Dot  :| [Dash, Dot, Dot])
                           , ('M', Dash :| [Dash])
                           , ('N', Dash :| [Dot])
                           , ('O', Dash :| [Dash, Dash])
                           , ('P', Dot  :| [Dash, Dash, Dot])
                           , ('Q', Dash :| [Dash, Dot, Dash])
                           , ('R', Dot  :| [Dash, Dot])
                           , ('S', Dot  :| [Dot, Dot])
                           , ('T', Dash :| [])
                           , ('U', Dot  :| [Dot, Dash])
                           , ('V', Dot  :| [Dot, Dot])
                           , ('W', Dot  :| [Dash, Dash])
                           , ('X', Dash :| [Dot, Dot, Dash])
                           , ('Y', Dash :| [Dot, Dash, Dash])
                           , ('Z', Dash :| [Dash, Dot, Dot])
                           ]

charToMorseLetter :: Char -> Maybe MorseLetter
charToMorseLetter c = Map.lookup (toUpper c) charCodeMapping

strToMorseWord :: String -> Maybe MorseWord
strToMorseWord [] = Nothing
strToMorseWord (l:ls) = charToMorseLetter l >>= (\ml ->
                                                   traverse charToMorseLetter ls >>= (\mls ->
                                                                                        Just (ml :| mls)))

strToMorseCode :: String -> Maybe MorseCode
strToMorseCode [] = Nothing
strToMorseCode s = case words s of
  [] -> Nothing
  (w:ws) -> strToMorseWord w >>= (\mw ->
                                    traverse strToMorseWord ws >>= (\mws ->
                                                                      Just (mw :| mws)))

main :: IO ()
main = getArgs <&> (!! 0) >>= putStrLn . mc2str . fromJust . strToMorseCode
