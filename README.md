# Morse

This is a small application to demonstrate some common Haskell idioms.  It takes
a string of characters as input and outputs a sequence of dots and dashes
representing the Morse code for those characters.

## Running

Run the program using Cabal:

    $ cabal run text-to-morse "Hello world"
	···· · ·-·· ·-·· ---    ·-- --- ·-· ·-·· -··

Note that only ASCII characters are supported: no numbers and no punctuation
allowed.
